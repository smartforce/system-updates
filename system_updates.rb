#!/usr/bin/env ruby
#

require 'rubygems'
# Used for Configuration
require 'yaml'
# https://github.com/piotrmurach/tty-prompt
require 'tty-prompt'
# Colors (https://github.com/piotrmurach/pastel#3-supported-colors)
require 'pastel'
# https://github.com/djberg96/ptools
require 'ptools'

# Load our class
require_relative './system_updates.class.rb'

# finally, run the damn thing!
SystemUpdates.new.run
